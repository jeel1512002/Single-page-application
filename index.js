const highlight = document.querySelector(".nav-left>svg");
const titleName = document.getElementById("title-name");

document.querySelector(".nav-left").addEventListener("mouseover", (e) => {
    if (e.target.tagName == "P" || e.target.tagName == "path") {
        titleName.style.color = "rgb(62, 131, 251)";
        highlight.style.stroke = "rgb(62, 131, 251)";
    }
});

document.querySelector(".nav-left").addEventListener("mouseout", (e) => {
    if (e.target.tagName == "P" || e.target.tagName == "path") {
        titleName.style.color = "rgb(237, 91, 52)";
        highlight.style.stroke = "rgb(237, 91, 52)";
    }
});

document.querySelector(".service-slider").addEventListener("mouseover", (e) => {
    stopSlider();
});

document.querySelector(".service-slider").addEventListener("mouseout", (e) => {
    startSlider();
});


document.getElementById("home").addEventListener("mouseover", () => {
    document.getElementById("home-link").classList.toggle("active", true);
    document.getElementById("technology-link").classList.toggle("active", false);
    document.getElementById("service-link").classList.toggle("active", false);
    document.getElementById("about-link").classList.toggle("active", false);
});

document.getElementById("technology").addEventListener("mouseover", () => {
    document.getElementById("home-link").classList.toggle("active", false);
    document.getElementById("technology-link").classList.toggle("active", true);
    document.getElementById("service-link").classList.toggle("active", false);
    document.getElementById("about-link").classList.toggle("active", false);
});
document.getElementById("service").addEventListener("mouseover", () => {
    document.getElementById("home-link").classList.toggle("active", false);
    document.getElementById("technology-link").classList.toggle("active", false);
    document.getElementById("service-link").classList.toggle("active", true);
    document.getElementById("about-link").classList.toggle("active", false);
});
document.getElementById("about").addEventListener("mouseover", () => {
    document.getElementById("home-link").classList.toggle("active", false);
    document.getElementById("technology-link").classList.toggle("active", false);
    document.getElementById("service-link").classList.toggle("active", false);
    document.getElementById("about-link").classList.toggle("active", true);
});


const typer = document.querySelector(".home-left>h2>span");
const typerArr = typer.innerHTML.split(",");

let printArr = [];

writeTyper();

setInterval(() => {
    typer.innerHTML = "";
    writeTyper();
}, (typerArr.toString().length + 1) * 150 * 2);

const cardArr = document.querySelectorAll(".project-card");
const prev = document.getElementById("prev");
const next = document.getElementById("next");

let position = 0;

if (!position) {
    prev.classList.toggle("remove", true);
}

if (position < (cardArr.length - 4) * -20) {
    next.classList.toggle("remove", true);
}

prev.onclick = prevFun;
next.onclick = nextFun;

let flag = false;
let time;
startSlider();

function delay(time = 150) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, time);
    });
}

async function writeTyper() {
    for (let word of typerArr) {
        printArr = [];
        for (let char of word) {
            await delay();
            printArr.push(char);
            typer.innerHTML = printArr.join("");
        }

        let i = word.length - 1;

        while (i >= 0) {
            printArr.pop();
            await delay();
            typer.innerHTML = printArr.join("");
            i--;
        }
    }
}

function startSlider() {
    time = setInterval(() => {
        if (position && flag) {
            prevFun();
        } else {
            flag = false;
            if (position >= (cardArr.length - 4) * -20 && !flag) {
                nextFun();
            } else {
                flag = true;
            }
        }
    }, 1500);
}


function stopSlider() {
    clearInterval(time);
}


function prevFun() {
    position += 20;
    if (!position) {
        prev.classList.toggle("remove", true);
    } else {
        prev.classList.toggle("remove", false);
    }

    if (position > (cardArr.length - 3) * 20) {
        next.classList.toggle("remove", true);
    } else {
        next.classList.toggle("remove", false);
    }
    cardArr.forEach((e) => {
        e.style.transform = `translateX(${position}rem)`;
    });
}

function nextFun() {
    position -= 20;
    if (!position) {
        prev.classList.toggle("remove", true);
    } else {
        prev.classList.toggle("remove", false);
    }

    if (position < (cardArr.length - 4) * -20) {
        next.classList.toggle("remove", true);
    } else {
        next.classList.toggle("remove", false);
    }
    cardArr.forEach((e) => {
        e.style.transform = `translateX(${position}rem)`;
    });
}
